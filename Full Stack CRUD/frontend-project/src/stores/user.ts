import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import userService from "@/services/user";
import type User from "@/types/User";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useUserStore = defineStore("User", () => {
  const loadingStore = useLoadingStore();
  const messStore = useMessageStore();
  const dialog = ref(false);
  const users = ref<User[]>([]);
  const editedUser = ref<User>({username:"", password: "" });

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if(!newDialog){
      editedUser.value = {username:"", password: "" };
    }
  })
  async function getUsers() {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUsers();
      users.value = res.data;
    } catch (e) {
      console.log(e);
      messStore.showError("Cannot pull data users");
    }
    loadingStore.isLoading = false;
  }
  const saveUser = async ()=>{
    loadingStore.isLoading = true;
    try{
      if(editedUser.value.id){
        const res = await userService.updateUser(editedUser.value.id, editedUser.value);

      }else{
        const res = await userService.saveUser(editedUser.value);


      }
      dialog.value = false;
      await getUsers();
    }catch(e){
      messStore.showError("Cannot save data users");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  async function deleteUser(id:number){
    loadingStore.isLoading = true;
    try{ 
        const res = await userService.deleteUser(id);

      await getUsers();
    }catch(e){
      messStore.showError("Cannot delete data users");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }
  function editUser(user:User){
    editedUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  }
  return { users, getUsers, dialog, editedUser,saveUser, editUser, deleteUser };
});