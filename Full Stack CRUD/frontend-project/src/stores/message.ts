import { ref, computed } from "vue";
import { defineStore } from "pinia";

export const useMessageStore = defineStore("message", () => {
  const isShow = ref(false);
  const messgae = ref("");
 
function showError(text: string ){
  messgae.value=text;
  isShow.value = true;
}
  return { isShow, messgae, showError   };
});
